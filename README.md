# StreamBoxOne
### Raspberry Pi 3+ powered Streaming-Machine

## Installation
Create image with [LibreElec SD Creator](https://wiki.libreelec.tv/de/libreelec_usb-sd_creator) on SD-card for you Raspberry.

Insert it into the Pi and boot.

It should boot and ask you for configurations.


## Configuration
Install Milhouse Testbuild

Go to **Settings** -> **LibreElec**

On page **system** -> **Updates**

Set update to manual

Add custom channel: http://milhouse.libreelec.tv/builds/master/RPi2

Update this channel

Look after lates release, update and the pi should restart

## Installing Prime Video
Go to **Addons** and look after the **Kodinerds**-Repo and install it

Then look after the **Sandman**-Repo and install it

After that look in the Sandman-Repo under **Video-Addons** for the Amazon Prime Video Addon and install it

## Prime Video Configuration
Open the addon and enter your credentials.

When you launch a video, you will be prompted to install WideVine. Do it. It will need about 2GB of space.

The Pi can only handle max 720p streams. To enable them go to the Addon-Settings, then Inputstream Addon Settings.

I gave maximal min. Bandwith and maximal max. Bandwith, the resolution fo the decoder was set to 720p in the general and secure decoder.

---
Everything should now work, so have fun!

Hint: I really recommend the Pellucid-skin for the kodi-interface






## Sources
\[1] LibreElec: <https://wiki.libreelec.tv/de/libreelec_usb-sd_creator>

\[2] Milhouse: <https://christian-brauweiler.de/libreelec-netflix-amazon-prime-video-skygo/>

\[3] PrimeVideo: <http://die25stestunde.de/libreelec-amazon-prime-video-mit-raspberry-pi-kodi-einrichten/>
